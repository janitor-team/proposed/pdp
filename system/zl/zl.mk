.PHONY: all clean run_test
all: run_test
clean:
	rm -f *.o test *~ *.so *.a

H = $(wildcard *.h)
C = $(wildcard *.c)
O = $(C:.c=.o)
CFLAGS := -O3 -fPIC -ffast-math -I.. -DHAVE_LIBV4L1_VIDEODEV_H -Wall #-Werror
%.o: %.c $(H)
	$(CC) $(CFLAGS) -c $< -o $@


# 2013/10/16 : Still in the process of merging PDP and libprim's dependencies on zl.
# Test is only for a subset of the objects:

TEST_O := v4l.o xwindow.o xv.o glx.o 3Dcontext_common.o 3Dcontext_glx.o test.o
LIBS := -lpthread -L/usr/X11R6/lib -lX11 -lXv -lXext -lGL -lGLU #-llua
test: $(TEST_O)
	$(CC) -o test $(TEST_O) $(LIBS) # $(TEST_LIBS) ??

LIB_C := \
	3Dcontext_common.c \
	3Dcontext_glx.c \
	glx.c \
	v4l.c \
	xv.c \
	xwindow.c \
	new.c
LIB_O := $(LIB_C:.c=.o)

libzl.so: $(LIB_O)
	$(CC) -shared -o $@ $(LIB_O) $(LIBS)

zl.a: $(LIB_O)
	$(AR) -r $@ $(LIB_O)

libzl.so: $(LIB_O)
	$(CC) -shared -o $@ $(LIB_O) $(LIBS)


run_test: test
	./test

