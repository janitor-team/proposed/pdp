#!/usr/bin/make -f

export PDP_EXTRA_CFLAGS = -fPIC -Wno-error $(CPPFLAGS) $(CFLAGS)
DPKG_EXPORT_BUILDFLAGS = 1
export DEB_BUILD_MAINT_OPTIONS=hardening=+all
include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/buildflags.mk


ifneq ($(filter amd64 i386,$(DEB_HOST_ARCH)),)
DEB_CONFIGURE_EXTRA_FLAGS = --enable-mmx
DEB_EXTRA_CFLAGS = -mmmx
BUILD_SCAF = yes
endif

%:
	dh $@

override_dh_autoreconf:
	dh_autoreconf --as-needed debian/autogen.sh

override_dh_auto_configure:
	dh_auto_configure -- $(DEB_CONFIGURE_EXTRA_FLAGS)
	dh_auto_configure -Dscaf -- $(DEB_CONFIGURE_EXTRA_FLAGS)

override_dh_auto_build:
	dh_auto_build -- pdp_all
	dh_auto_build --buildsystem=makefile --sourcedirectory=opengl -- \
		LDFLAGS="$(LDFLAGS)" \
		PDP_EXTRA_CFLAGS="$(CFLAGS) $(DEB_EXTRA_CFLAGS)" \
		PDP_EXTRA_CPPFLAGS="$(CPPFLAGS)"
ifneq ($(BUILD_SCAF),)
	dh_auto_build --sourcedirectory=scaf -- \
		LDFLAGS="-fPIC $(LDFLAGS)" \
		PDP_CFLAGS="$(CPPFLAGS) -fPIC $(CFLAGS)"
	cp scaf/rules/carules.scafo scaf/rules/default.scafo
endif
	docbook-to-man debian/pdp-config.sgml > pdp-config.1

override_dh_auto_clean:
	[ ! -f opengl/Makefile.config ] || make -C opengl clean
	[ ! -f scaf/Makefile.config ] || make -C scaf clean
	[ ! -f Makefile.config ] || make clean
	-$(RM) -rf bin/pdp-config include/pdp_config.h
	-$(RM) -f Makefile.config config.log config.status
	-$(RM) -f scaf/Makefile.config scaf/config.log scaf/config.status
	-$(RM) -f pdp-config.1
	-$(RM) -f scaf/rules/default.scafo

override_dh_installchangelogs:
	dh_installchangelogs CHANGES.LOG

override_dh_fixperms:
	dh_fixperms
	find debian -name '*.pd_linux' -exec \
		chmod 0644 {} +
	find debian -name '*.scafo' -exec \
		chmod 0644 {} +

licensecheck:
	licensecheck --deb-machine -r . \
		> debian/copyright_newhints
	cmp debian/copyright_hints debian/copyright_newhints \
		&& rm debian/copyright_newhints
